// ch_metadata_file = file(params.metadata_file, checkIfExists: true)
ch_input_folder = file(params.input_folder)
ch_celltype_annotation_file = file(params.celltype_annotation_file)
ch_seurat_file = file(params.seurat_file)

workflow ANNOTATION {
    ANNOTATION_PROCESS (
         ch_input_folder,
         ch_celltype_annotation_file,
         ch_seurat_file,
    )
}

process ANNOTATION_PROCESS {

    container "${(params.containers_local) ? 'semares_scrna_celltype_annotation_external:1.0.1' :'registry.gitlab.com/semares_pipelines/scrna_celltype_annotation_external/semares_scrna_celltype_annotation_external:1.0.1' }"

    input:
        path ch_input_folder
        path ch_celltype_annotation_file
        path ch_seurat_file

    output:
        path("output/*")
    
    script:
        """
        mkdir 'output'
        annotation.R \\
      	   --input_folder $ch_input_folder \\
           --seurat_file "$ch_seurat_file" \\
           --celltype_annotation_file "$ch_celltype_annotation_file" \\
           --celltype_annotation_file_barcode_column "${params.celltype_annotation_file_barcode_column}" \\
           --celltype_annotation_file_delimiter '${params.celltype_annotation_file_delimiter}' \\
           --celltype_annotation_file_celltype_column "${params.celltype_annotation_file_celltype_column}" \\
           --celltype_annotation_file_sample_name_column "${params.celltype_annotation_file_sample_name_column}" \\
           --celltype_annotation_file_barcode_strip ${params.celltype_annotation_file_barcode_strip} \\
           --celltype_annotation_file_barcode_strip_delimiter '${params.celltype_annotation_file_barcode_strip_delimiter}' \\
           --seurat_file_sample_name_column "${params.seurat_file_sample_name_column}" \\
           --sample_name "${params.sample_name}" \\
           --seurat_file_new_celltype_annotation_column "${params.seurat_file_new_celltype_annotation_column}" \\
           --seurat_file_barcode_strip "${params.seurat_file_barcode_strip}" \\
           --seurat_file_barcode_strip_delimiter '${params.seurat_file_barcode_strip_delimiter}' \\
           --not_annotated_string "${params.not_annotated_string}" \\
           --output_folder "./output"
        """  
}
